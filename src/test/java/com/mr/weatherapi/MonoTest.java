package com.mr.weatherapi;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

public class MonoTest {

    @Test
    void testMono() {
        getWeather();
    }

    @Test
    void testMonoWithBlock() {
        Mono<String> mono = getWeather();
        Mono<String> monoTest = mono.map(x -> {
            System.out.println("Mono test");
            return x;
        });

        monoTest.block();
        mono.block();
    }

    private Mono<String> getWeather() {
        return Mono.just("Beautiful weather in")
                .map(x -> x.concat(" Gdansk"))
                .map(x -> {
                    System.out.println("Show on std out: " + x);
                    return x;
                });
    }
}
