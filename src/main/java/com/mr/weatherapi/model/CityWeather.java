package com.mr.weatherapi.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class CityWeather implements Serializable {

    private String name;
    private int visibility;
    private List<Weather> weather;
    private Main main;
    private Wind wind;
}
