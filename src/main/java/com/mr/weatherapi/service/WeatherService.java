package com.mr.weatherapi.service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.mr.weatherapi.model.CityWeather;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class WeatherService {

    private final String key;
    private final WebClient webClient;
    private final HazelcastInstance hazelcastInstance;

    @Autowired
    public WeatherService(WebClient webClient,
                          @Value("${weather.api.key}") String key,
                          HazelcastInstance hazelcastInstance) {
        this.webClient = webClient;
        this.key = key;
        this.hazelcastInstance = hazelcastInstance;
    }

    public Mono<CityWeather> check(String city) {
        IMap<Object, Object> cache = hazelcastInstance.getMap("weather");
        if (cache.containsKey(city)) {
            log.info("Getting weather from cache for city: {}", city);
            return Mono.just((CityWeather) cache.get(city));
        }
        Mono<CityWeather> cityWeather = callApiWeather(city);
        return cityWeather.map(weather -> {
            cache.putIfAbsent(city, weather);
            return weather;
        });
    }

    public Mono<CityWeather> callApiWeather(String city) {
        log.info("Calling weather API for city {}", city);
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/weather")
                        .queryParam("q", city)
                        .queryParam("appid", key)
                        .queryParam("units", "metric")
                        .build())
                .retrieve()
                .bodyToMono(CityWeather.class);
    }
}
