package com.mr.weatherapi.service;

import com.hazelcast.cluster.Member;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import com.mr.weatherapi.model.CityWeather;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@EnableScheduling
@Slf4j
public class RefreshScheduler {

    private final WeatherService weatherService;
    private final HazelcastInstance hazelcastInstance;

    @Autowired
    public RefreshScheduler(WeatherService weatherService,
                            HazelcastInstance hazelcastInstance) {
        this.weatherService = weatherService;
        this.hazelcastInstance = hazelcastInstance;
    }

    @Scheduled(fixedDelay = 1800000)
    public void schedule() {
        if (isLeader()) {
            IMap<Object, Object> cache = hazelcastInstance.getMap("weather");
            cache.forEach((key, value) -> {
                String city = (String) key;
                try {
                    CityWeather weather = weatherService.callApiWeather(city).block();
                    cache.put(city, weather);
                } catch (Exception ex) {
                    log.error("Cannot fetch weather for city {}", city);
                }
            });
        }
    }

    private boolean isLeader() {
        Set<Member> members = hazelcastInstance.getCluster().getMembers();
        Member firstMember = members.iterator().next();
        return firstMember.localMember();
    }
}
