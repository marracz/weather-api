package com.mr.weatherapi.rest;

import com.mr.weatherapi.model.CityWeather;
import com.mr.weatherapi.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class WeatherController {

    private final WeatherService weatherService;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping(value = "/api/weather/{city}/check")
    public Mono<CityWeather> checkWeather(@PathVariable("city") String city) {
        return weatherService.check(city);
    }
}
