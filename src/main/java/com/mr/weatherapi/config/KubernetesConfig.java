package com.mr.weatherapi.config;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.util.ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class KubernetesConfig {

    @Value("${kubernetes.inCluster}")
    private boolean inCluster;

    @Value("${kubernetes.path}")
    private String apiServerPath;

    @Bean
    public ApiClient apiClient() throws IOException {
        return inCluster ?
                ClientBuilder.cluster().build() :
                ClientBuilder.standard().setBasePath(apiServerPath).build();
    }
}
