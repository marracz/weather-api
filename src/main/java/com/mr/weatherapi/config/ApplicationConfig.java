package com.mr.weatherapi.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class ApplicationConfig {

    @Value("${weather.api.address}")
    private String apiAddress;

    @Bean
    public WebClient webClient() {
        return WebClient.create(apiAddress);
    }
}
