package com.mr.weatherapi.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.mr.weatherapi.infrastructure.PodScanner;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Slf4j
@Configuration
public class HazelcastConfig {

    @Value("${kubernetes.labelSelector.key}")
    private String labelSelectorKey;

    @Value("${kubernetes.labelSelector.value}")
    private String labelSelectorValue;

    @Bean
    @ConditionalOnProperty(value = "hazelcast.discover", havingValue = "kubernetes")
    public HazelcastInstance kubernetesHazelcastInstance() {
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        Config config = hazelcastInstance.getConfig();
        config.getNetworkConfig().getJoin().getMulticastConfig().setEnabled(false);
        config.getNetworkConfig().getJoin().getKubernetesConfig().setEnabled(true)
                .setProperty("namespace", "KUBERNETES_NAMESPACE")
                .setProperty("service-name", labelSelectorValue);
        return hazelcastInstance;
    }

    @Bean
    @ConditionalOnProperty(value = "hazelcast.discover", havingValue = "custom-kubernetes")
    public HazelcastInstance customKubernetesHazelcastInstance(PodScanner podScanner) {
        try {
            HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
            Config config = hazelcastInstance.getConfig();
            NetworkConfig network = config.getNetworkConfig();
            JoinConfig join = network.getJoin();
            join.getMulticastConfig().setEnabled(false);

            V1PodList pods = podScanner.scanPodsWithLabelSelector(
                    String.format("%s=%s", labelSelectorKey, labelSelectorValue));

            TcpIpConfig tcpIpConfig = join.getTcpIpConfig().setEnabled(true);
            pods.getItems().stream()
                    .map(V1Pod::getStatus)
                    .filter(Objects::nonNull)
                    .filter(status -> "Running".equals(status.getPhase()))
                    .forEach(status -> tcpIpConfig.addMember(status.getPodIP()));
            return hazelcastInstance;
        } catch (ApiException e) {
            log.error("Cannot fetch pods from Kubernetes API server - {}", e.getResponseBody(), e);
            throw new RuntimeException("Cannot start application", e);
        }
    }

}

