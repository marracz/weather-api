package com.mr.weatherapi.infrastructure;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1PodList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PodScanner {

    private final ApiClient apiClient;

    @Autowired
    public PodScanner(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public V1PodList scanPodsWithLabelSelector(String labelSelector) throws ApiException {
        return new CoreV1Api(apiClient)
                .listPodForAllNamespaces(null, null,
                        null, labelSelector,
                        null, null,
                        null, null,
                        null, null);
    }
}
