#!/bin/bash
set -xv

echo -n "Starting application: "

exec java $JAVA_OPTS -jar $JAR --spring.config.name=$CONFIG_NAME $SPRING_OPTS
