# weather-api

Application fetches weather data from `https://api.openweathermap.org` API and
store it in distributed map. 
Data in cache are refreshing by scheduler that run once per configurable frequency.

Application is prepared to run in Kubernetes environment.

Application should be run with `hazelcast.discovery` property:

* `kubernetes` value means standard Hazelcast configuration of Hazelcast cluster instances discovery by service endpoints 
* `custom-kubernetes` value means custom implementation of Hazelcast cluster instances discovery by fetching pods with specified labels and label selector.

``` kubectl create serviceaccount weather-api```

``` kubectl create clusterrolebinding weather-api-pods-view --clusterrole=view --serviceaccount=default:weather-api```